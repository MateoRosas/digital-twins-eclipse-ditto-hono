# Digital Twins Eclipse Ditto - HONO

Semillero IoT
1. Instalación de Eclipse Ditto
- Ir a la página [](https://www.eclipse.org/ditto/)
- Acceder al github de Ditto [](https://github.com/eclipse/ditto)
- Para iniciar clonamos el repositorio y compilamos las siguientes lineas dentro del directorio: 

`$ cd /<yourCustomDir>/ditto/deployment/docker`

`$ docker-compose up -d`

![Alt text](/Imagenes/cmd_ditto_docker.PNG?raw=true "cmd")

Docker tendría el siguiente proyecto montado:

![Alt text](/Imagenes/ditto_docker.PNG?raw=true "cmd")

Con esto, ya tenemos instalado Eclipse Ditto, podemos verificarlo accediendo a `http://localhost:8080`

![Alt text](/Imagenes/localhost_ditto.PNG?raw=true "cmd")

Desde esta página podemos acceder a un Eclipse Ditto HTTP API, el cual nos permitirá administrar, crear las estructuras, crear políticas, buscar y enviar mensajes a un objeto.

![Alt text](/Imagenes/2api_ditto.PNG?raw=true "api http")

En cada pestaña, aparecen las peticiones http que por medio de curl (interprete de comando basado en la transferencia de archivos) especifican exactamente los métodos POST, GET, PUT entre otros, para la administración de objetos. Esta API nos proporciona las diferentes respuestas que pueden aparecer cuando generamos peticiones.(descripcion de los errores y su código) 

![Alt text](/Imagenes/2api_comands_ditto.PNG?raw=true "comands")

Para configurar el usuario podemos ir a esta parte del HTTP API:

![Alt text](/Imagenes/2api_authorize_ditto.PNG? raw=true "user")

Aquí podemos configurar el acceso del usuario para hacer la administración de los objetos.


_Ejemplo:_

A continuación, registraremos un objeto por medio de una petición http, para saber si el computador tiene instalado curl se compila la siguiente linea `curl --help`

Si no, en este link está el tutorial para instalar curl: [](https://www.youtube.com/watch?v=di2lymA1Vb8) 


- Registrar un objeto


**Parte 1**

Para registrar un objeto es necesario hacer una petición http por medio de un POST

![Alt text](/Imagenes/create_thing_part1.PNG? raw=true "Part 1")


**Parte 2**

![Alt text](/Imagenes/create_thing_part2.PNG? raw=true "Part 2")

**Parte 3**

![Alt text](/Imagenes/create_thing_part3.PNG? raw=true "Part 3")

**Parte 4**

![Alt text](/Imagenes/create_thing_part4.PNG? raw=true "Part 4")

**Parte 5**

![Alt text](/Imagenes/objeto_ditto_cmd.PNG? raw=true "Part 5")


**Resultado**

![Alt text](/Imagenes/response_peticionhttp.PNG? raw=true "Resultado")


**Pedir el objeto**

![Alt text](/Imagenes/response_peticionhttp_put.PNG? raw=true "Petición del objeto")

**Resultado**

![Alt text](/Imagenes/response_peticionhttp_put_html.PNG? raw=true "Resultado PUT")

**Pedir la poltica del objeto**´

![Alt text](/Imagenes/response_peticionhttp_policy.PNG? raw=true "Petición del objeto")






2. Descarga del código abierto de Eclipse Ditto front-end

En el siguiente enlace se encuetra el código de Eclipse Ditto: https://github.com/eclipse/ditto-examples/tree/master/mqtt-bidirectional

En la carpeta de mqtt-bidirectional hay dos directorios:
- iot-device / octopus
- iot-frontend

En este proyecto se utilizará el directorio de iot-frontend donde se integra una interfaz gráfica el cual permite manejar los dispositivos físicos, siendo más exactos nos permite añadir dispositivos iot como los sensores para su administración. Nos permitirá agregar un id, una política, unos atributos y el tipo de dispositivo, el cual permite sincronizar los dispositiovs del mundo real y físicos.

El directorio iot-device/octopus utiliza un script para comunicar Eclipse Ditto con un dispositivo iot siendo una placa Octopus ESP8266 que tiene varios sensores integrados por ende no se utilizará en este proyecto.

2. Instalación de Eclipse Ditto

En primer lugar hay que tener en cuenta que necesitamos lanzar la interfaz web de forma local, para ello se realizan los siguientes pasos:

- Descargar la carpeta mqtt-bidirectional
- Abrimos el editor de código Visual Studio Code y la carpeta descargada
- Abrimos la terminal cmd desde Visual Code

3. 1. Ejecutar la interfaz gráfica de Eclipse Ditto

Tenemos que configurar el proyecto en OS windows 

- Abrimos la terminal cmd en la carpeta `mqtt-bidirectional/iot-frontend`
- Configurar el proyecto se utiliza el siguiente comando `npm install`
- Compilar y recargar en "caliente" para el desarrollo `npm run serve`

Con estos comandos podemos ejecutar la interfaz Eclipse Ditto, además podemos revisar los archivos 'README.md' para arreglar o minificar el proyecto para la producción.

Siguiendo los pasos anteriores, debemos obtener el siguiente resultado:

![Alt text](/Imagenes/interfaz_ditto_noconnect.jpg?raw=true "interfaz Ditto")


4. Instalar el broker Eclipse Mosquitto

Mosquitto es una implementación de código abierto de un servidor para las versiones 5.0, 3.1.1 y 3.1 del protocolo MQTT. Para su implementación utilizaremos Docker, el cual es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos.

Para la instalación de Docker (en windows), seguimos estos pasos:

- Instalar Docker en este link [Docker](https://www.docker.com/products/docker-desktop)
- Windows pide instalar WSL2 Linux Kernel [WSL2 Linux Kernel](https://docs.microsoft.com/en-us/windows/wsl/install-win10#step-4---download-the-linux-kernel-update-package)


Es necesario utilizar Windows PowerShell con los privilegios de administrador para el servidor de prueba de Mosquitto.

- Ejecutamos el siguiente código en el cmd: `docker run -d -p 1883:1883 -p 9001:9001 eclipse-mosquitto`

![Alt text](/Imagenes/docker_install_mosquitto.PNG?raw=true "Ejemplo")

- La interfaz docker debe tener montado la imagen de Mosquitto

![Alt text](/Imagenes/docker_image_update.PNG?raw=true "Interfaz Docker")
